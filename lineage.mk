# Inherit from the common Open Source product configuration
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base_telephony.mk)

# Inherit device configuration
$(call inherit-product, device/samsung/i9082/device.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

## Device identifier. This must come after all inclusions
PRODUCT_NAME := lineage_i9082
PRODUCT_DEVICE := i9082
PRODUCT_BRAND := samsung
PRODUCT_MANUFACTURER := Samsung
PRODUCT_MODEL := GT-I9082

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME=baffinxx \
    TARGET_DEVICE=baffin \
    PRIVATE_BUILD_DESC="baffinxx-user 4.2.2 JDQ39 I9082XXUBMJ1 release-keys"
    
# Set BUILD_FINGERPRINT variable to be picked up by both system and vendor build.prop
    BUILD_FINGERPRINT="samsung/baffinxx/baffin:4.2.2/JDQ39/I9082XXUBMJ1:user/release-keys"
